# SQL in SAS

In this program, I am demonstrating how to use SQL within SAS to analyze a csv data set about baseball players.

Each table title explains what each piece of code is extracting and creating:

1. Top 10 home run hitters.
2. Total number of hits from the first 10 players
3. Players who make between 1 and 2 million dollars
4. Players who made major league minimum and atleast 100 singles
5. Average of home runs separated by whether the minimum was met or not
6. Low, medium, and high amounts of doubles and how many in each group.

> I have also included the results of the program in a pdf file.