data assignment10;
infile '/folders/myfolders/CSCI3113/xcelfiles_csvfiles/Baseball.csv' dlm = ',' dsd firstobs=2;
LENGTH Player $15;
input Player $ Salary Min $ G PA AB ONEB TWOB THREEB HR BB HBP SO SB CS;
RUN;

proc sql outobs=10;
	title 'top 10 hr hitters';
	select Player as Player, HR as HR
	from work.assignment10
	order by HR desc;
	RUN;
	
proc sql outobs=10;
	title 'total number of hits for the first 10 players';
	select Player as Player, SUM(ONEB,TWOB,THREEB,HR) as TotalHits
	from work.assignment10;
	run;
	
proc sql;
	title'these players make between 1 and 2 million dollars';
	select Player as Player,Salary as Salary
	from work.assignment10
	where Salary > .99 and Salary < 2.01
	order by Salary desc;
	run;
	
proc sql;
	title 'dataset of players who make between 1 and 2 million dollars a year';
	create table work.Players_between_1and2_million like
	work.assignment10;
	select * from work.assignment10
	where Salary > .99 and Salary < 2.01
	order by Salary desc;
	run;
	
proc sql;
	title 'Players who made the major league minimum and had atleast 100 singles';
	create table work.minimum like
	work.assignment10;
	select * from work.assignment10
	where Min = 'YES' and ONEB > 99
	order by ONEB desc;
	run;
	
proc sql;
	title'Average of Home Runs separated by whether the Minimum was met or not';
	select avg(HR) as averageHR, Min as MINMET
	from work.assignment10
	group by Min;
	run;

proc sql;
	title 'Low, medium, and high amounts of doubles and how many in each group';
	create table doublescount as 
	select Player,TWOB,
	case when TWOB < 10 then 'low'
	when TWOB > 9 and TWOB < 20 then 'medium' else 'high'
	end as double
	from work.assignment10;
	run;
proc sql;
	select Player, TWOB, double, 
	count(case when (double='high'or 'medium' or 'low') then double end) as doubletotal
	from doublescount
	group by double;
run;


	